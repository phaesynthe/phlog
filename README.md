# Phlog #

The Phaesynthe log utility.

## Getting Started ##

## Examples ##

```
const Phlog = require('phlog')
log = new Phlog('example module')

log.notice('example method', 'message')
```
