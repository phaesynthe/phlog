'use strict';

const { createLogger, format, transports } = require('winston');
const { colorize, combine, timestamp, printf } = format;

const logLevel = process.env.LOGLEVEL || 'silly';

const messageFormat = printf(({ level, message, timestamp }) => {
  return `${timestamp} ${level}: ${message}`;
});

const LogLevelValues = {
  error: 1,
  warn: 2,
  info: 3,
  http: 4,
  verbose: 5,
  debug: 6,
  silly: 7
};

/**
 * An opinionated wrapper for the winston logger.
 * @type {Logger}
 * @param {function} error
 * @param {function} warn
 * @param {function} info
 * @param {function} http
 * @param {function} verbose
 * @param {function} debug
 * @param {function} silly
 */
const logger = createLogger({
  level: logLevel,
  format: combine(
    colorize(),
    timestamp(),
    messageFormat
  ),
  transports: [
    new transports.Console()
  ]
});

class Phlog {
  constructor(serviceName) {
    this._serviceName = `${serviceName}`;
    this._moduleLogLevelValue = LogLevelValues[logLevel] || 0;
  }

  /**
   * Provides unobstructed access to the underlying logger as configured
   *
   * @returns {Logger}
   */
  getRawLogger() {
    return logger;
  }

  /**
   * Provides a contextualized logger to streamline logging.
   *
   * @param {string} serviceName
   * @param {string} moduleLogPrefix
   * @param {string} [moduleLogLevel] Indicate that this module should log at a more detailed level than the application. A value that is less detailed is functionally ignored.
   * @returns {Logger}
   */
  getModuleLogger(serviceName, moduleLogPrefix, moduleLogLevel = logLevel) {
    /** @type {Logger} */
    const modularizedLogger = {
      error: ()=> {},
      warn: ()=> {},
      info: ()=> {},
      http: ()=> {},
      verbose: ()=> {},
      debug: ()=> {},
      silly: ()=> {}
    };

    this._moduleLogLevel = moduleLogLevel;

    // For each supported log level, we map the logger level to a proxy level
    for (const attribute of Object.keys(modularizedLogger)) {
      modularizedLogger[attribute] = (message) => {
        if (LogLevelValues[attribute] <= LogLevelValues[this._moduleLogLevel]) {
          // The proxy level automatically prepends the module name to the logged message.
          logger[attribute](`${moduleLogPrefix}: ${message}`);
        }
      };
    }

    return modularizedLogger;
  }

  async shutDown() {
    // No Op
  }
}

module.exports = Phlog;

