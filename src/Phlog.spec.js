/* global jest */

'use strict';

const { expect } = require('chai');
const Phlog = require('./Phlog');

const winston = require('winston');

describe('Phlog', () => {

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('can be instantiated', () => {
    expect(typeof Phlog, ' Correct type').equals('function');

    const phlog = new Phlog();
    expect(typeof phlog, 'creates an object').equals('object');
    expect(winston.createLogger.mock.calls.length).equals(1);
  });

  it('getRawLogger() provides access to the underlying logger', () => {
    const phlog = new Phlog();
    expect(typeof phlog.getRawLogger, 'method exists').equals('function');

    const log = phlog.getRawLogger();
    log.error('getRawLogger() blah');

    expect(winston.rawLogMock.error.mock.calls.length).equals(1);
  });

  it('getModuleLogger() provides contextualized access to the underlying logger', () => {
    const fakeName = 'FAKE_NAME';
    const fakePrefix = 'FAKE_PREFIX';

    const phlog = new Phlog();
    expect(typeof phlog.getModuleLogger, 'method exists').equals('function');

    const log = phlog.getModuleLogger(fakeName, fakePrefix);
    log.error('getModuleLogger() blah');

    expect(winston.rawLogMock.error.mock.calls.length).equals(1);
    const message = winston.rawLogMock.error.mock.calls[0][0];

    expect(message.indexOf(fakePrefix)).equals(0);
    expect(message.indexOf('blah')).greaterThan(1);
  });

  it('shutDown() exists', () => {
    const phlog = new Phlog();
    expect(typeof phlog.shutDown, 'method exists').equals('function');

    phlog.shutDown( );
  });

});
