/* global jest */
'use strict';

const rawLogMock = {
  error: jest.fn(),
  warn: jest.fn(),
  info: jest.fn(),
  http: jest.fn(),
  verbose: jest.fn(),
  debug: jest.fn(),
  silly: jest.fn()
};

module.exports = {
  createLogger: jest.fn(() => { return rawLogMock; }),
  format: {
    colorize: jest.fn,
    combine: jest.fn(),
    printf: jest.fn(),
    timestamp: jest.fn()
  },
  transports: {
    Console: jest.fn()
  },

  rawLogMock
};
